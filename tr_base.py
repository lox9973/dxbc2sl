# SPDX-License-Identifier: MIT
# SPDX-FileCopyrightText: © 2023 lox9973
import re
import logging
from typing import Iterator, Protocol
from abc import abstractmethod
from parser import Parser, split_mask, contain_expr, parse_int_operand

class Template(Protocol):
	def format(self, *args: object, **kwargs: object) -> str:
		pass

class Translator(Parser):
	state_opcode: str = "nop"
	state_sample_c: set[str] = set()

	def do_instruction(self, opcode: str, operands: list[str]) -> Iterator[str]:
		# split instruction with two dsts
		if re.search(r"^(sincos|imul|umul|udiv)$", opcode):
			for i in range(2):
				if operands[i^1] == "null" or not any(contain_expr(x, operands[i^0]) for x in operands[2:]):
					for j in range(2):
						if operands[i^j] != "null":
							yield self._do_instruction(opcode, f".{i^j}", [operands[i^j]]+operands[2:])
					return
			raise NotImplementedError("both outputs are used in inputs")
		# extract gather4 channel
		elif opcode.startswith("gather4"):
			operands = list(operands)
			sampler_idx = 4 if opcode.startswith("gather4_po") else 3
			operands[sampler_idx], mask = split_mask(operands[sampler_idx])
			yield self._do_instruction(opcode, mask or ".x", operands)
		# extract rasterizer
		elif opcode.startswith("sample") and operands[1].startswith("rasterizer"):
			# we don't treat "rasterizer" as a resource so we assume its swizzle is standard
			yield self._do_instruction(opcode, ".rasterizer", operands)
		# optimize mov float
		elif opcode.startswith("mov") and all(x.find(".") >= 0 for x in operands[1:]): # no int constant
			yield self._do_instruction(opcode, ".float", operands)
		# case fallthrough
		elif opcode == "case" and self.state_opcode == "case":
			yield self._do_instruction(opcode, ".fallthrough", operands)
		else:
			yield self._do_instruction(opcode, "", operands)

	def _do_instruction(self, opcode: str, selector: str, operands: list[str]) -> str:
		opcode_base = re.sub(r"_sat$|\([-\w,]+\)", "", opcode) # remove _sat and (u,v,w)(tex)(type,type,type,type)
		# https://github.com/baldurk/renderdoc/blob/35b13a8/renderdoc/driver/shaders/dxbc/dxbc_bytecode_ops.cpp#L2927
		saturated = bool(re.search(r"_sat$", opcode))
		# https://github.com/baldurk/renderdoc/blob/35b13a8/renderdoc/driver/shaders/dxbc/dxbc_bytecode_ops.cpp#L2827
		if aoffimmi := re.search(r"\(([0-9-]+),([0-9-]+),([0-9-]+)\)", opcode):
			opcode_base += "_aoffimmi"
			operands.append(f"l({aoffimmi[1]}, {aoffimmi[2]}, {aoffimmi[3]})")

		# wrap switch case with braces (for wgsl)
		if self.state_opcode in ["case", "default", "switch"]:
			prefix = "{" if opcode not in ["case", "default"] else ""
		else:
			prefix = "}" if opcode in ["case", "default", "endswitch"] else ""
		self.state_opcode = opcode

		# function opcode
		template = self.template_func_op.get(opcode_base+selector) or self.template_func_op.get(opcode_base)
		if template is not None:
			if opcode_base+selector not in self.template_func_op:
				selector = "" # don't forget to fallback
			dst_operand, src_operands = operands[0], operands[1:]
			# component-wise func
			dst_mask = split_mask(dst_operand)[1] or ".x"
			comp_wise = not re.search(r"\.([xyzw]{4}|rrrr)$", str(template)) # doesn't end with swizzle
			# resource func
			idxs = self.template_op_attrs.get(opcode_base+selector, {})
			idx_res = idxs.get("resource")
			if idx_res is not None:
				src_operands[idx_res], mask = split_mask(src_operands[idx_res]) # detach swizzle
				res_swizzle = mask or ".x"
				res_swizzle += res_swizzle[-1] * (5-len(res_swizzle)) # expand
				res_type = self.dcl_resource[src_operands[idx_res]][0]
				if re.search(r"_c(\b|_)", opcode_base):
					self.state_sample_c.add(src_operands[idx_res]) # mark cmp for wgsl/glsl

			dst_expr = self.do_dst_operand(dst_operand)
			src_exprs = [self.do_src_operand(x) for x in src_operands]
			# restrict to mask: comp_wise
			if comp_wise:
				select2or3 = len(dst_mask) > 2 and len(dst_mask) < 5 # opt
				for i, src in enumerate(src_operands):
					if select2or3 or src.find(",") >= 0: # unless it's already scalar
						src_exprs[i] += dst_mask
			# apply resource operand template
			elif idx_res is not None:
				src_exprs.append("")
				for name, idx in idxs.items():
					if name != "resource":
						src_exprs[idx] = self.template_res_src[name][res_type].format(*src_exprs[idx:])
			expr = template.format(*src_exprs)
			if idx_res is not None and not expr.endswith(".rrrr"):
				expr += res_swizzle # attach swizzle unless it's scalar
			if comp_wise:
				# NOTE: fix integer negate modifier on iadd,imad,imax,imin,imul,itof
				expr = expr.replace(f"{self.cast_int_negate}((-", f"(-{self.cast_int_negate}(")
				expr = re.sub(r"(float|int|bool)N", rf"\g<1>{len(dst_mask)-1 if len(dst_mask) > 2 else ''}", expr)
			# restrict to mask: not comp_wise
			elif len(dst_mask) <= 2 and expr.endswith(".rrrr"):
				expr = expr[:-5] # remove scalar swizzle to avoid syntax error in wgsl/glsl
			elif len(dst_mask) < 5: # opt
				expr += dst_mask
			return prefix + self.do_assign(dst_expr, f"saturate({expr})" if saturated else expr)

		# control opcode
		template = self.template_ctrl_op.get(opcode_base+selector) or self.template_ctrl_op.get(opcode_base)
		if template is not None:
			src_operands = [self.do_src_operand(x) for x in operands]
			if opcode_base == "case":
				src_operands[0] = str(parse_int_operand(operands[0]))
			return prefix + template.format(*src_operands)

		logging.warning(f"not supported: {opcode}{selector} {operands}")
		return prefix + f"// {opcode}{selector} {operands}"

	def do_dst_operand(self, operand: str) -> str:
		return re.sub(r"(?<=\[)\D.*?(?=(?: *\+ *\d+)?\])", rf"{self.cast_dynamic_index}(\g<0>)", operand)

	def do_src_operand(self, operand: str) -> str:
		if re.search(r"^l\(.*\)$", operand):
			return self.do_imm_operand(operand)
		# https://github.com/baldurk/renderdoc/blob/35b13a8e/renderdoc/driver/shaders/dxbc/dxbc_bytecode_ops.cpp#LL572C19-L572C19
		# TODO: handle precision
		if re.search(r"^-", operand):
			return f"(-{self.do_src_operand(operand[1:])})"
		if re.search(r"^\|.*\|$", operand):
			return f"abs({self.do_src_operand(operand[1:-1])})"
		return self.do_dst_operand(operand)

	@classmethod
	def do_imm_operand(self, operand: str) -> str:
		# https://github.com/baldurk/renderdoc/blob/35b13a8/renderdoc/driver/shaders/dxbc/dxbc_stringise.cpp#L502
		s = operand[2:-1].split(", ")
		if operand.find(".") >= 0: # valid float (or zero)
			imm_type = "float"
		else:
			imm_type = "uint" if operand.find("0x") >= 0 else "int" # TODO: check int range
		return f"{imm_type}{len(s) if len(s) > 1 else ''}({', '.join(s)})"

	@classmethod
	def do_assign(self, dst: str, src: str, free_mask: bool=True) -> str:
		dst_base, mask = split_mask(dst)
		if mask == ".xyzw":
			return f"{dst_base} = {src};"
		elif len(mask) <= 2 or free_mask:
			return f"{dst} = {src};"
		else:
			return f"{dst_base} = float4{template_assign[mask].format(dst_base, src)};"

	prelude: list[str]
	template_func_op: dict[str, Template]
	template_ctrl_op: dict[str, Template]
	template_op_attrs: dict[str, dict[str, int]]
	template_res_src: dict[str, dict[str, Template]]
	cast_dynamic_index: str
	cast_int_negate: str

	@abstractmethod
	def do_declaration(self) -> Iterator[str]:
		raise NotImplementedError
	@abstractmethod
	def do_header(self) -> Iterator[str]:
		raise NotImplementedError
	@abstractmethod
	def do_footer(self) -> Iterator[str]:
		raise NotImplementedError

template_assign = {
	".xy": "({1}, {0}.zw)",
	".xz": "({1}, {0}.yw).xzyw",
	".xw": "({1}, {0}.yz).xzwy",
	".yz": "({0}.xw, {1}).xzwy",
	".yw": "({0}.xz, {1}).xzyw",
	".zw": "({0}.xy, {1})",
	".xyz": "({1}, {0}.w)",
	".xyw": "({1}, {0}.z).xywz",
	".xzw": "({0}.y, {1}).yxzw",
	".yzw": "({0}.x, {1})",
}

def cast_template_args(templates: dict[str, Template], spec: list[tuple[str,str,str]]) -> Iterator[tuple[str,Template]]:
	for prefix, cast_src, cast_dst in spec:
		for k, tmpl in templates.items():
			x = re.sub(r"\{\d\}", rf"{cast_src}(\g<0>)" if cast_src else r"\g<0>", str(tmpl))
			yield (prefix+k, x if x.startswith(cast_dst) else f"{cast_dst}({x})")

def extract_template_attributes(templates: dict[str, Template]) -> dict[str, dict[str, int]]:
	attrs: dict[str, dict[str, int]] = {}
	for k, tmpl in templates.items():
		templates[k] = re.sub(r"\{(\d)\.(\w+)\}", lambda m:
			f"{{{attrs.setdefault(k, {}).setdefault(m[2], int(m[1]))}}}", str(tmpl))
	return attrs