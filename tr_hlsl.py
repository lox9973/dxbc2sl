# SPDX-License-Identifier: MIT
# SPDX-FileCopyrightText: © 2023 lox9973
import re
from typing import Iterator

from tr_base import Translator, Template, cast_template_args, extract_template_attributes

class HLSLTranslator(Translator):
	cast_dynamic_index = "asuint"
	cast_int_negate = "asint"

	def __init__(self):
		super().__init__()
		self.prelude = prelude
		self.template_func_op = template_func_op
		self.template_ctrl_op = template_ctrl_op
		self.template_op_attrs = template_op_attrs
		self.template_res_src = template_res_src

	def do_declaration(self) -> Iterator[str]:
		if icb := self.dcl_immediateConstantBuffer:
			yield f"const static float4 icb[] = {{{', '.join(self.do_imm_operand(x) for x in icb)}}};"
		for name, (size, access) in self.dcl_constantbuffer.items():
			yield f"uniform float4 {name}[{size}];"
		for name, (res_type, ret_type, size) in self.dcl_resource.items():
			yield f"uniform {resource_types[res_type]} {name};" # TODO: size
		for name, (mode,) in self.dcl_sampler.items():
			yield f"uniform {sampler_types[mode]} {name};"
		for name, (mask, sv, interp) in self.dcl_input.items():
			yield f"static {'float4' if mask else 'float'} {name};"
		for name, (mask, sv) in self.dcl_output.items():
			yield f"static {'float4' if mask else 'float'} {name};"

	def do_header(self) -> Iterator[str]:
		yield f"void {self.shader_stage}_main() {{"
		yield "const int MAX_LOOP = 64;"
		yield "float4 TMP;" # used by GetDimensions
		if self.dcl_temps:
			yield "float4 %s;" % ", ".join(f"r{i}" for i in range(self.dcl_temps))
		for name, (size, comp) in self.dcl_indexableTemp.items():
			yield f"float{comp} {name}[{size}];"

	def do_footer(self) -> Iterator[str]:
		yield "}"
		yield ""

		input_decls = []
		input_assigns = []
		for name, (mask, sv, interp) in self.dcl_input.items():
			if info := input_types.get(f"{sv or name}.{self.shader_stage}") or input_types.get(sv or name):
				sv, type = info
				vname = f"int(v.{name})" if type == "bool" else f"v.{name}"
				input_decls.append(f"{interp_modifiers[interp] if interp else ''}{type} {name} : {sv};")
				input_assigns.append(f"{name} = asfloat({vname});")

		output_decls = []
		output_assigns = []
		for name, (mask, sv) in self.dcl_output.items():
			if info := output_types.get(f"{sv or name}.{self.shader_stage}") or output_types.get(sv or name):
				sv, type = info
				output_decls.append(f"{type} {name} : {sv};")
				output_assigns.append(f"o.{name} = as{type.rstrip('4')}({name});")

		if input_decls:
			yield f"struct {self.shader_stage}_input {{"
			yield from input_decls
			yield "};"
		if output_decls:
			yield f"struct {self.shader_stage}_output {{"
			yield from output_decls
			yield "};"

		yield f"{f'{self.shader_stage}_output' if output_decls else 'void'} main({f'{self.shader_stage}_input v' if input_decls else ''}) {{"
		yield from input_assigns
		yield f"{self.shader_stage}_main();"
		if output_decls:
			yield f"{self.shader_stage}_output o = ({self.shader_stage}_output)0;"
		yield from output_assigns
		if output_decls:
			yield "return o;"
		yield "}"

prelude: list[str] = [
]
template_func_op: dict[str, Template] = {
	# mov
	"mov":  "asfloat({0})",
	"movc": "(asuint({0}) ? asfloat({1}) : asfloat({2}))",
	"mov.float":  "{0}",
	"movc.float": "(asuint({0}) ? {1} : {2})",

	# cast
	"f16tof32": "f16tof32(asuint({0}))",
	"f32tof16": "asfloat(f32tof16({0}))",
	"ftoi": "asfloat(intN({0}))",
	"ftou": "asfloat(uintN({0}))",
	"itof": "floatN(asint({0}))",
	"utof": "floatN(asuint({0}))",

	# floatN => floatN
	"deriv_rtx":        "ddx({0})",
	"deriv_rtx_coarse": "ddx_coarse({0})",
	"deriv_rtx_fine":   "ddx_fine({0})",
	"deriv_rty":        "ddy({0})",
	"deriv_rty_coarse": "ddy_coarse({0})",
	"deriv_rty_fine":   "ddy_fine({0})",
	"exp":      "exp2({0})",
	"frc":      "frac({0})",
	"log":      "log2({0})",
	"rcp":      "rcp({0})",
	"round_ne": "round({0})",
	"round_ni": "floor({0})",
	"round_pi": "ceil({0})",
	"round_z":  "trunc({0})",
	"rsq":      "rsqrt({0})",
	"sincos.0": "sin({0})",
	"sincos.1": "cos({0})",
	"sqrt":     "sqrt({0})",
	# floatN => float
	"dp2": "dot({0}.xy, {1}.xy).rrrr",
	"dp3": "dot({0}.xyz, {1}.xyz).rrrr",
	"dp4": "dot({0}.xyzw, {1}.xyzw).rrrr",

	**dict(cast_template_args({
		# TN => TN
		"add": "({0} + {1})",
		"div": "({0} / {1})",
		"div.0": "({0} / {1})",
		"div.1": "({0} % {1})",
		"mad": "mad({0}, {1}, {2})",
		"max": "max({0}, {1})",
		"min": "min({0}, {1})",
		"mul": "({0} * {1})",
		"mul.0": "({0} - {0})", # HACK
		"mul.1": "({0} * {1})",
		# TN => boolN
		"eq": "asfloat({0} == {1} ? -1 : 0)",
		"ge": "asfloat({0} >= {1} ? -1 : 0)",
		"lt": "asfloat({0} <  {1} ? -1 : 0)",
		"ne": "asfloat({0} != {1} ? -1 : 0)",
	}, [("", "", ""), ("u", "asuint", "asfloat"), ("i", "asint", "asfloat")])),

	# intN => intN
	"bfrev":        "asfloat(reversebits(asuint({0})))",
	"countbits":    "asfloat(countbits(asuint({0})))",
	"firstbit_hi":  "asfloat(firstbithigh(asuint({0})))",
	"firstbit_lo":  "asfloat(firstbitlow(asuint({0})))",
	"firstbit_shi": "asfloat(firstbithigh(asint({0})))",
	"ineg":   "asfloat(-asint({0}))",
	"not":    "asfloat(~asuint({0}))",
	"and":    "asfloat(asuint({0}) & asuint({1}))",
	"ishl":   "asfloat(asint({0}) << asuint({1}))",
	"ishr":   "asfloat(asint({0}) >> asuint({1}))",
	"or":     "asfloat(asuint({0}) | asuint({1}))",
	"ushr":   "asfloat(asuint({0}) >> asuint({1}))",
	"xor":    "asfloat(asuint({0}) ^ asuint({1}))",

	# resource
	"ld":          "{1.resource}.Load({0.address_ld}).xyzw",
	"ldms":        "{1.resource}.Load({0.address_ld}, asint({2})).xyzw",
	"sample":      "{1.resource}.Sample({2}, {0.address}).xyzw",
	"sample_b":    "{1.resource}.SampleBias({2}, {0.address}, {3}).xyzw",
	"sample_d":    "{1.resource}.SampleGrad({2}, {0.address}, {3.ddx}, {4.ddy}).xyzw",
	"sample_l":    "{1.resource}.SampleLevel({2}, {0.address}, {3}).xyzw",
	"sample_c":    "{1.resource}.SampleCmp({2}, {0.address}, {3}).rrrr",
	"sample_c_lz": "{1.resource}.SampleCmpLevelZero({2}, {0.address}, {3}).rrrr",
	"gather4.x":   "{1.resource}.Gather({2}, {0.address}).xyzw",
	"gather4.y":   "{1.resource}.GatherGreen({2}, {0.address}).xyzw",
	"gather4.z":   "{1.resource}.GatherBlue({2}, {0.address}).xyzw",
	"gather4.w":   "{1.resource}.GatherAlpha({2}, {0.address}).xyzw",
	"gather4_c.x": "{1.resource}.GatherCmp({2}, {0.address}, {3}).xyzw",
	"gather4_c.y": "{1.resource}.GatherCmpGreen({2}, {0.address}, {3}).xyzw",
	"gather4_c.z": "{1.resource}.GatherCmpBlue({2}, {0.address}, {3}).xyzw",
	"gather4_c.w": "{1.resource}.GatherCmpAlpha({2}, {0.address}, {3}).xyzw",
	"gather4_po.x":   "{2.resource}.Gather({3}, {0.address}, {1.offset}).xyzw",
	"gather4_po.y":   "{2.resource}.GatherGreen({3}, {0.address}, {1.offset}).xyzw",
	"gather4_po.z":   "{2.resource}.GatherBlue({3}, {0.address}, {1.offset}).xyzw",
	"gather4_po.w":   "{2.resource}.GatherAlpha({3}, {0.address}, {1.offset}).xyzw",
	"gather4_po_c.x": "{2.resource}.GatherCmp({3}, {0.address}, {4}, {1.offset}).xyzw",
	"gather4_po_c.y": "{2.resource}.GatherCmpGreen({3}, {0.address}, {4}, {1.offset}).xyzw",
	"gather4_po_c.z": "{2.resource}.GatherCmpBlue({3}, {0.address}, {4}, {1.offset}).xyzw",
	"gather4_po_c.w": "{2.resource}.GatherCmpAlpha({3}, {0.address}, {4}, {1.offset}).xyzw",
	"lod": "float4({1.resource}.CalculateLevelOfDetail({2}, {0.ddx}), {1.resource}.CalculateLevelOfDetailUnclamped({2}, {0.ddx}), 0, 0).xyzw",

	# resource aoffimmi
	"ld_aoffimmi":          "{1.resource}.Load({0.address_ld}, {2.offset}).xyzw",
	"ldms_aoffimmi":        "{1.resource}.Load({0.address_ld}, asint({2}), {3.offset}).xyzw",
	"sample_aoffimmi":      "{1.resource}.Sample({2}, {0.address}, {3.offset}).xyzw",
	"sample_b_aoffimmi":    "{1.resource}.SampleBias({2}, {0.address}, {3}, {4.offset}).xyzw",
	"sample_d_aoffimmi":    "{1.resource}.SampleGrad({2}, {0.address}, {3.ddx}, {4.ddy}, {5.offset}).xyzw",
	"sample_l_aoffimmi":    "{1.resource}.SampleLevel({2}, {0.address}, {3}, {4.offset}).xyzw",
	"sample_c_aoffimmi":    "{1.resource}.SampleCmp({2}, {0.address}, {3}, {4.offset}).rrrr",
	"sample_c_lz_aoffimmi": "{1.resource}.SampleCmpLevelZero({2}, {0.address}, {3}, {4.offset}).rrrr",
	"gather4_aoffimmi.x":   "{1.resource}.Gather({2}, {0.address}, {3.offset}).xyzw",
	"gather4_aoffimmi.y":   "{1.resource}.GatherGreen({2}, {0.address}, {3.offset}).xyzw",
	"gather4_aoffimmi.z":   "{1.resource}.GatherBlue({2}, {0.address}, {3.offset}).xyzw",
	"gather4_aoffimmi.w":   "{1.resource}.GatherAlpha({2}, {0.address}, {3.offset}).xyzw",
	"gather4_c_aoffimmi.x": "{1.resource}.GatherCmp({2}, {0.address}, {3}, {4.offset}).xyzw",
	"gather4_c_aoffimmi.y": "{1.resource}.GatherCmpGreen({2}, {0.address}, {3}, {4.offset}).xyzw",
	"gather4_c_aoffimmi.z": "{1.resource}.GatherCmpBlue({2}, {0.address}, {3}, {4.offset}).xyzw",
	"gather4_c_aoffimmi.w": "{1.resource}.GatherCmpAlpha({2}, {0.address}, {3}, {4.offset}).xyzw",

	# resource info
	"resinfo":         "({1.resource}.GetDimensions({0.resinfo0}, TMP.w), TMP).xyzw",
	"resinfo_uint":    "({1.resource}.GetDimensions({0.resinfo0}, TMP.w), asfloat(uint4(TMP))).xyzw",
	"sampleinfo":      "({0.resource}.GetDimensions({1.resinfo0}, TMP.w), float4(TMP.w, 0, 0, 0)).xyzw",
	"sampleinfo_uint": "({0.resource}.GetDimensions({1.resinfo0}, TMP.w), float4(asfloat(uint(TMP.w)), 0, 0, 0)).xyzw",
	"sampleinfo.rasterizer":      "float4(float(GetRenderTargetSampleCount()), 0, 0, 0).xyzw",
	"sampleinfo_uint.rasterizer": "float4(asfloat(GetRenderTargetSampleCount()), 0, 0, 0)).xyzw",
	"samplepos.rasterizer":       "float4(GetRenderTargetSamplePosition(asint({1})), 0, 0).xyzw",
	"samplepos":                  "float4({0.resource}.GetSamplePosition(asint({1})), 0, 0).xyzw",
}
template_op_attrs = extract_template_attributes(template_func_op)

template_ctrl_op: dict[str, Template] = {
	"nop": "",

	"loop": "{{[loop] for (int I=0; I<MAX_LOOP; I++) {{",
	"endloop": "}}}}",

	"if_z": "if (!asuint({0})) {{",
	"if_nz": "if (asuint({0})) {{",
	"else": "}} else {{",
	"endif": "}}",

	"switch": "[forcecase] switch (asuint({0})) {{",
	"case": "case {0}:",
	"default": "default:",
	"endswitch": "}}",
}
# add break etc
for opc, st in [("breakc","break;"), ("continuec","continue;"), ("retc","return;"), ("discard","discard;")]:
	template_ctrl_op.update({
		opc.rstrip('c'): st,
		f"{opc}_z": "if (!asuint({0})) {{ "+st+" }}",
		f"{opc}_nz": "if (asuint({0})) {{ "+st+" }}",
	})

template_res_src: dict[str, dict[str, Template]] = {
	# https://learn.microsoft.com/en-us/windows/win32/direct3dhlsl/dx-graphics-hlsl-to-load
	"address_ld": {
		"texture1d":        "asint({0}.xw)",
		"texture1darray":   "asint({0}.xyw)",
		"texture2d":        "asint({0}.xyw)",
		"texture2darray":   "asint({0}.xyzw)",
		"texture2dms":      "asint({0}.xy)",
		"texture2dmsarray": "asint({0}.xyz)",
		"texture3d":        "asint({0}.xyzw)",
	},
	# https://learn.microsoft.com/en-us/windows/win32/direct3dhlsl/dx-graphics-hlsl-to-sample
	"address": {
		"texture1d":        "{0}.x",
		"texture1darray":   "{0}.xy",
		"texture2d":        "{0}.xy",
		"texture2darray":   "{0}.xyz",
		"texture3d":        "{0}.xyz",
		"texturecube":      "{0}.xyz",
		"texturecubearray": "{0}.xyzw",
	},
	# https://learn.microsoft.com/en-us/windows/win32/direct3dhlsl/dx-graphics-hlsl-to-load
	"offset": {
		"texture1d":        "asint({0}.x)",
		"texture1darray":   "asint({0}.x)",
		"texture2d":        "asint({0}.xy)",
		"texture2darray":   "asint({0}.xy)",
		"texture2dms":      "asint({0}.xy)",
		"texture2dmsarray": "asint({0}.xy)",
		"texture3d":        "asint({0}.xyz)",
	},
	# https://learn.microsoft.com/en-us/windows/win32/direct3dhlsl/dx-graphics-hlsl-to-samplegrad
	**{k: {
		"texture1d":        "{0}.x",
		"texture1darray":   "{0}.x",
		"texture2d":        "{0}.xy",
		"texture2darray":   "{0}.xy",
		"texture3d":        "{0}.xyz",
		"texturecube":      "{0}.xyz",
		"texturecubearray": "{0}.xyz",
	} for k in ["ddx", "ddy"]},
	# https://learn.microsoft.com/en-us/windows/win32/direct3dhlsl/dx-graphics-hlsl-to-getdimensions
	"resinfo0": {
		"texture1d":        "asuint({0}), TMP.x",
		"texture1darray":   "asuint({0}), TMP.x, TMP.y",
		"texture2d":        "asuint({0}), TMP.x, TMP.y",
		"texture2darray":   "asuint({0}), TMP.x, TMP.y, TMP.z",
		"texture2dms":      "TMP.x, TMP.y",
		"texture2dmsarray": "TMP.x, TMP.y, TMP.z",
		"texture3d":        "asuint({0}), TMP.x, TMP.y, TMP.z",
		"texturecube":      "asuint({0}), TMP.x, TMP.y",
		"texturecubearray": "asuint({0}), TMP.x, TMP.y, TMP.z",
	},
}
# https://learn.microsoft.com/en-us/windows/win32/direct3dhlsl/d3d11-graphics-reference-sm5-objects
resource_types = {
	"texture1d":        "Texture1D<float4>",
	"texture1darray":   "Texture1DArray<float4>",
	"texture2d":        "Texture2D<float4>",
	"texture2darray":   "Texture2DArray<float4>",
	"texture2dms":      "Texture2DMS<float4>",
	"texture2dmsarray": "Texture2DMSArray<float4>",
	"texture3d":        "Texture3D<float4>",
	"texturecube":      "TextureCube<float4>",
	"texturecubearray": "TextureCubeArray<float4>",
}
# https://learn.microsoft.com/en-us/windows/win32/direct3dhlsl/dx-graphics-hlsl-sampler
sampler_types = {
	"default": "SamplerState",
	"comparison": "SamplerComparisonState",
}
# https://learn.microsoft.com/en-us/windows/win32/direct3dhlsl/dx-graphics-hlsl-struct
interp_modifiers = {
	"constant":        "nointerpolation ",
	"linear":          "linear ",
	"linear centroid": "linear centroid ",
	"linear sample":   "linear sample ",
	"linear noperspective":          "linear noperspective ",
	"linear noperspective centroid": "linear noperspective centroid ",
	"linear noperspective sample":   "linear noperspective sample ",
}
# https://learn.microsoft.com/en-us/windows/win32/direct3dhlsl/dx-graphics-hlsl-semantics
# TODO: compute shader
input_types = {
	**{f"v{i}":      (f"TEXCOORD{i}", "float4") for i in range(16)},
	"position":      ("SV_Position", "float4"),
	"vertexid":      ("SV_VertexID", "uint"), 
	"instanceid":    ("SV_InstanceID", "uint"),
	"isfrontface":   ("SV_IsFrontFace", "bool"),
	"sampleidx":     ("SV_SampleIndex", "uint"),
	"vCoverageMask": ("SV_Coverage", "uint"),
	"vPrim":         ("SV_PrimitiveID", "uint"),
	"clipdistance":  ("SV_ClipDistance", "float4"),
	"culldistance":  ("SV_CullDistance", "float4"),
	"rendertarget_array_index": ("SV_RenderTargetArrayIndex", "uint"),
	"viewport_array_index":     ("SV_ViewportArrayIndex", "uint"),
}
output_types = {
	**{f"o{i}":      (f"TEXCOORD{i}", "float4") for i in range(16)},
	**{f"o{i}.ps":   (f"SV_Target{i or ''}", "float4") for i in range(16)},
	"position":      ("SV_Position", "float4"),
	"oMask":         ("SV_Coverage", "uint"),
	"oDepth":        ("SV_Depth", "float"),
	"oDepthGreaterEqual": ("SV_DepthGreaterEqual", "float"),
	"oDepthLessEqual":    ("SV_DepthLessEqual", "float"),
	"oStencilRef":        ("SV_StencilRef", "uint"),
}