# SPDX-License-Identifier: MIT
# SPDX-FileCopyrightText: © 2023 lox9973
import struct
from io import BufferedIOBase
from wasmer import engine, wasi, Store, Module, ImportObject, Instance # type: ignore
from wasmer_compiler_cranelift import Compiler # type: ignore

def seek_dxbc(file: BufferedIOBase, n: int=1024):
	file.seek(file.tell() + max(0, file.read(n).find(b'DXBC')))

def load_dxbc_chunks(file: BufferedIOBase) -> dict[str,bytes]:
	base = file.tell()
	sig, csum, one, file_size, chunk_cnt = struct.unpack("<4s16sIII", file.read(4+16+12))
	assert sig == b'DXBC'
	chunks = {}
	fmt = struct.Struct("<I")
	for offset in [x[0] for x in fmt.iter_unpack(file.read(fmt.size*chunk_cnt))]:
		file.seek(base+offset)
		chunk_sig, chunk_size = struct.unpack("<4sI", file.read(8))
		chunks[chunk_sig] = file.read(chunk_size)
	return chunks

class DXBCDisassembler:
	def __init__(self, wasm_data: bytes):
		store = Store(engine.Universal(Compiler))
		module = Module(store, wasm_data)
		wasi_env = wasi.StateBuilder('dxbc').finalize()
		import_object = wasi_env.generate_import_object(store, wasi.get_version(module, strict=True))
		instance = Instance(module, import_object)
		self.e = instance.exports

	def run(self, data: bytes) -> str:
		data_len = len(data)
		data_ptr: int = self.e.malloc(data_len)
		memoryview(self.e.memory.buffer)[data_ptr:data_ptr+data_len] = data
		text_ptr: int = self.e.DXBCBytecode_Program_GetDisassembly(data_ptr, data_len)
		text_len: int = self.e.strlen(text_ptr)
		try:
			return bytes(memoryview(self.e.memory.buffer)[text_ptr:text_ptr+text_len]).decode()
		finally:
			self.e.free(text_ptr)

def main():
	import argparse
	parser = argparse.ArgumentParser()
	parser.add_argument("input")
	parser.add_argument("--wasm", default="renderdoc.min.wasm")
	args = parser.parse_args()
	with open(args.input, "rb") as f:
		seek_dxbc(f)
		chunks = load_dxbc_chunks(f)
	with open(args.wasm, "rb") as f:
		disassembler = DXBCDisassembler(f.read())
	data = chunks.get(b"SHDR") or chunks.get(b"SHEX")
	print(disassembler.run(data), end="")

if __name__ == '__main__':
	main()