# SPDX-License-Identifier: MIT
# SPDX-FileCopyrightText: © 2023 lox9973
import re
import struct
import logging
from typing import Iterator
from abc import ABCMeta

class Parser(metaclass=ABCMeta):
	shader_model: str = ""
	dcl_globalFlags: list[str] = []
	dcl_constantbuffer: dict[str, tuple[int, str]] = {}
	dcl_immediateConstantBuffer: list[str] = []
	dcl_sampler: dict[str, tuple[str]] = {}
	dcl_resource: dict[str, tuple[str, str, int]] = {}
	dcl_input: dict[str, tuple[str, str|None, str|None]] = {}
	dcl_output: dict[str, tuple[str, str|None]] = {}
	dcl_temps: int = 0
	dcl_indexableTemp: dict[str, tuple[int, int]] = {}

	@property
	def shader_stage(self):
		return self.shader_model[:2]

	@classmethod
	def parse_lines(self, lines: Iterator[str]) -> Iterator[tuple[str, list[str]]]:
		for line in lines:
			if m := re.search(r"^[\s\d:]*(\w\S+)(?: (.*\S))?", line):
				opcode, rem = m[1], m[2]
				# remove _indexable: inconsistent in renderdoc
				# remove _aoffimmi: not used by renderdoc
				# remove _float: not used by fxc
				# remove _ in sample_info and ld_ms: fix renderdoc alias
				opcode = re.sub(r"_indexable(\b|(?=_))|_aoffimmi|_float$|(?<=sample)_(?=info)|(?<=ld)_(?=ms)", "", opcode)
				# https://github.com/baldurk/renderdoc/blob/3f7e648/renderdoc/driver/shaders/dxbc/dxbc_bytecode_ops.cpp#L1765
				if rem == "{": # dcl_immediateConstantBuffer
					for line in lines:
						rem += (line := line.strip())
						if line.endswith("}"):
							break
					yield (opcode, [rem])
				elif rem is None:
					yield (opcode, [])
				else:
					operands = re.split(r",? " if is_dcl_op(opcode) else r", (?![^(]*\))", rem)
					for i, operand in enumerate(operands):
						if re.search(r"^l\(0(, 0)*\)$", operand):
							operands[i] = operand.replace("0", "0.0") # treat zero scalar/vector as float
					if opcode.startswith("dcl_input_ps"):
						for i, operand in enumerate(operands):
							if operand == "nointerpolation":
								operands[i] = "constant" # fix renderdoc alias
					yield (opcode, operands)

	def parse_declaration(self, opcode: str, operands: list[str]):
		if re.search(r"\ws_", opcode):
			self.shader_model = opcode
		elif opcode == "dcl_globalFlags":
			self.dcl_globalFlags = operands
		# buffer
		elif opcode == "dcl_constantbuffer":
			m = re.search(r"^(\w+)\[(\d+)\]", operands[0])
			assert m
			self.dcl_constantbuffer[m[1]] = (int(m[2]), operands[1])
		elif opcode == "dcl_immediateConstantBuffer":
			self.dcl_immediateConstantBuffer = [f"l({x.strip('{} ')})" for x in re.split(r"\},\s*\{", operands[0])]
		# resource/sampler
		elif opcode == "dcl_sampler":
			self.dcl_sampler[operands[0]] = (operands[1][5:],)
		elif opcode.startswith("dcl_resource_"):
			m = re.search(r"^(\w+)(?:\((\d+)\))?", opcode[13:])
			assert m
			self.dcl_resource[operands[1]] = (m[1], operands[0], int(m[2] or "0"))
		# input/output
		elif opcode.startswith("dcl_input") or opcode.startswith("dcl_output"):
			idx, sv = -1, None
			if re.search(r"_s[ig]v$", opcode):
				idx, sv = -2, operands[-1]
			name, mask = split_mask(operands[idx])
			if opcode.startswith("dcl_output"):
				self.dcl_output[name] = (mask, sv)
			elif opcode.startswith("dcl_input_ps"):
				self.dcl_input[name] = (mask, sv, " ".join(operands[:idx]))
			else:
				self.dcl_input[name] = (mask, sv, None)
		# temp
		elif opcode == "dcl_temps":
			self.dcl_temps = int(operands[0])
		elif opcode == "dcl_indexableTemp":
			m = re.search(r"^(\w+)\[(\d+)\]$", operands[0])
			assert m
			self.dcl_indexableTemp[m[1]] = (int(m[2]), int(operands[1]))
		else:
			logging.warning(f"not supported: {opcode} {operands}")

def split_mask(x: str) -> tuple[str, str]:
	m = re.search(r"\.[xyzw]+$", x)
	return (x[:-len(m[0])], m[0]) if m else (x, "")

def contain_expr(x: str, y: str) -> bool:
	y, _ = split_mask(y)
	return bool(re.search(rf"\b{re.escape(y)}\b", x))

def parse_int_operand(operand: str) -> int:
	x = operand[2:-1]
	return struct.unpack("i", struct.pack("f", float(x)))[0] if x.find(".") >= 0 else int(x)

def is_dcl_op(opcode: str) -> bool:
	# https://github.com/baldurk/renderdoc/blob/3f7e648/renderdoc/driver/shaders/dxbc/dxbc_bytecode.cpp#L985
	return bool(re.search(r"^(\ws_\d|dcl_|hs_decls)", opcode))