# dxbc2sl

This Python tool translates Direct3D shader bytecode (SM4/5) into HLSL, WGSL and GLSL ES3. It calls the disassembler from RenderDoc, which is compiled into WebAssembly, and translates its output assembly to shader code in target language.

## Usage

Disassemble a dxbc binary, and translate into WGSL:

```
dxbc2asm.py ps.dxbc > ps.asm
dxasm2sl.py ps.asm wgsl > ps.wgsl
```

## Requirement

[wasmer](https://github.com/wasmerio/wasmer-python) is used to run wasm. The RenderDoc wasm is provided, or you can also build it from scratch with the script in `/wasm`.

## Supported instruction

```
add
and
bfrev
break
breakc
case
continue
continuec
countbits
dcl_constantbuffer
dcl_globalFlags
dcl_immediateConstantBuffer
dcl_indexableTemp
dcl_input[_sgv|_siv]
dcl_output[_sgv|_siv]
dcl_resource
dcl_sampler
dcl_temps
default
deriv_rtx[_coarse|_fine]
deriv_rty[_coarse|_fine]
discard
div
dp2
dp3
dp4
else
endif
endloop
endswitch
eq
exp
f16tof32
f32tof16
firstbit[_hi|_lo|_shi]
frc
ftoi
ftou
gather4[_po][_c][_aoffimmi]
ge
iadd
ieq
if
ige
ilt
imad
imax
imin
imul
ine
ineg
ishl
ishr
itof
ld[_aoffimmi]
ldms[_aoffimmi]
lod
log
loop
lt
mad
max
min
mov
movc
mul
ne
nop
not
or
rcp
resinfo[_uint]
ret
retc
round[_ne|_ni|_pi|_z]
rsq
sample[_b|_c|_c_lz|_d|_l][_aoffimmi]
sampleinfo[_uint]
samplepos
sincos
sqrt
switch
udiv
uge
ult
umad
umax
umin
umul
ushr
utof
xor
```
