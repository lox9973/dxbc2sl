# SPDX-License-Identifier: MIT
# SPDX-FileCopyrightText: © 2023 lox9973
import re
import logging
import itertools
from typing import Iterator

from parser import is_dcl_op
from tr_base import Translator
from tr_hlsl import HLSLTranslator
from tr_wgsl import WGSLTranslator
from tr_glsl import GLSLES3Translator

def remove_ps(opcode: str, operands: list[str]) -> tuple[str, list[str]]:
	if re.search(r"^sample\b", opcode):
		return "sample_l", operands+["l(0.0)"]
	elif opcode.startswith("sample_b"):
		return "sample_l", operands
	elif opcode.startswith("sample_c"):
		return "sample_c_lz", operands
	elif opcode == "discard":
		return "ret", operands
	elif opcode.startswith("discard_"):
		return "retc_"+opcode[8:], operands
	elif opcode.startswith("deriv_"):
		return "mov", [operands[0], "l(0.0, 0.0, 0.0, 0.0)"]
	return opcode, operands

def translate(trans: Translator, lines: Iterator[str], no_ps: bool=False) -> list[str]:
	outputs: list[str] = []
	dcl = True
	for opcode, operands in trans.parse_lines(lines):
		if dcl and is_dcl_op(opcode):
			trans.parse_declaration(opcode, operands)
		else:
			dcl = False
			if no_ps:
				opcode, operands = remove_ps(opcode, operands)
			outputs.extend(trans.do_instruction(opcode, operands))
	return [*trans.prelude, "", *trans.do_declaration(), "", *trans.do_header(), *outputs, *trans.do_footer()]

def pretty_lines(lines: list[str]):
	swizzle_merge = load_swizzle_merge()
	indent = 0
	for i, line in enumerate(lines):
		# remove trailing zeros
		line = re.sub(r"(\d)\.0+(\b|(?=e))", r"\1", line)
		line = re.sub(r"(\d\.\d+?)0+(\b|(?=e))", r"\1", line)
		# merge swizzles
		line = re.sub(r"\.xyzw\b", "", line)
		line = re.sub(r"\.[xyzw]+\.[xyzw]+\b", lambda m: swizzle_merge[m[0]], line)
		# indent
		if line.startswith("}") and (len(line) <= 2 or line.endswith("{")):
			indent -= 1
		lines[i] = "\t"*indent + line
		if line.endswith("{"):
			indent += 1

def load_swizzle_merge():
	pairs = [("".join("xyzw"[x] for x in fun), fun)\
		for fun in itertools.chain(*(itertools.product(range(4), repeat=n) for n in range(1,5)))]
	merge = {}
	for sw0, fun0 in pairs:
		for sw1, fun1 in pairs:
			if all(x < len(fun0) for x in fun1):
				merge[f".{sw0}.{sw1}"] = "." + "".join("xyzw"[fun0[x]] for x in fun1)
	return merge

def main():
	import argparse
	parser = argparse.ArgumentParser()
	parser.add_argument('input')
	parser.add_argument('lang', choices=['hlsl', 'wgsl', 'glsl'])
	parser.add_argument('--no_ps', action='store_true')
	args = parser.parse_args()

	lang = args.lang.lower()
	trans = {"hlsl": HLSLTranslator, "wgsl": WGSLTranslator, "glsl": GLSLES3Translator}[lang]()

	with open(args.input, "r") as f:
		outputs = translate(trans, iter(f), no_ps=args.no_ps)
	pretty_lines(outputs)
	for line in outputs:
		print(line)

if __name__ == '__main__':
	main()