// minimal definition to make d3dcommon.h work
#pragma once

// winerror.h
typedef int             HRESULT;

// basetyps.h
#ifdef __cplusplus
# define EXTERN_C extern "C"
#else
# define EXTERN_C extern
#endif
#define STDMETHODCALLTYPE 
# define STDMETHOD(m) HRESULT (STDMETHODCALLTYPE *m)
# define STDMETHOD_(t,m) t (STDMETHODCALLTYPE *m)
# define PURE
# define THIS_ INTERFACE *,
# define THIS INTERFACE *

// guiddef.h
typedef struct {} GUID;
#define DEFINE_GUID(...)
typedef GUID IID,*LPIID;
#define REFIID const IID &

// rpcdce.h
typedef void* RPC_IF_HANDLE;

// basetsd.h
typedef long          LONG_PTR, *PLONG_PTR;
typedef unsigned long ULONG_PTR, *PULONG_PTR;
typedef LONG_PTR SSIZE_T, *PSSIZE_T;
typedef ULONG_PTR SIZE_T, *PSIZE_T;

// windef.h
typedef void                                   *LPVOID;
typedef const void                             *LPCVOID;
typedef int             BOOL,       *PBOOL,    *LPBOOL;
typedef unsigned char   BYTE,       *PBYTE,    *LPBYTE;
typedef int             INT,        *PINT,     *LPINT;
typedef unsigned int    UINT,       *PUINT;
typedef unsigned int    ULONG,      *PULONG;

// winnt.h
#ifndef VOID
#define VOID void
#endif
typedef BYTE            BOOLEAN,    *PBOOLEAN;
typedef char            CHAR,       *PCHAR;
typedef const CHAR     *PCSTR,      *LPCSTR;

// objbase.h
#define CONST_VTBL const
#define interface struct
#define DECLARE_INTERFACE(iface)        interface iface##_
#define BEGIN_INTERFACE
#define END_INTERFACE

// misc
#define _COM_Outptr_
#define _In_
#define _Out_
#define CINTERFACE