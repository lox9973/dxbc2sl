#include <stdint.h>
#include <stdio.h>
#include "driver/shaders/dxbc/dxbc_bytecode.h"

extern "C" {
char* DXBCBytecode_Program_GetDisassembly(const byte *bytes, size_t length) {
	auto program = new DXBCBytecode::Program(bytes, length);
	auto output = program->GetDisassembly();
	auto outstr = (char*)malloc(output.length()+1);
	strcpy(outstr, output.c_str());
	return outstr;
}
}

int main(int argc, char *argv[]) {
	auto infile = fopen(argv[1], "r");
	fseek(infile, 0L, SEEK_END);
	auto numbytes = ftell(infile);
	fseek(infile, 0L, SEEK_SET);  
	auto buffer = (byte*)calloc(numbytes, sizeof(byte)); 
	fread(buffer, sizeof(byte), numbytes, infile);
	fclose(infile);
	auto outstr = DXBCBytecode_Program_GetDisassembly(buffer, numbytes);
	printf("%s", outstr);
	free(outstr);
	free(buffer);
	return 0;
}