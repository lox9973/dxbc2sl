#include "shim.h"
#include "common/common.h"
#include "core/settings.h"
#include "os/os_specific.h"
#include "driver/shaders/dxbc/dxbc_bytecode.h"

unsigned char _BitScanForward(unsigned long * Index, unsigned long Mask) {
	*Index = __builtin_ffs(Mask)-1;
	return 1;
}

// api/replay/renderdoc_replay.h
extern "C" RENDERDOC_API void RENDERDOC_CC RENDERDOC_FreeArrayMem(void *mem) {
	free(mem);
}
extern "C" RENDERDOC_API void *RENDERDOC_CC RENDERDOC_AllocArrayMem(uint64_t sz) {
	return malloc((size_t)sz);
}

// common/common.h
void rdcassert(char const*, char const*, unsigned int, char const*) {
}
void rdclog_flush() {
}
void rdclog_direct(time_t utcTime, uint32_t pid, LogType type, const char *project,
	const char *file, unsigned int line, const char *fmt, ...) {
}

// core/settings.h
ConfigVarRegistration<bool>::ConfigVarRegistration(rdcliteral name, const bool &defaultValue,
	bool debugOnly, rdcliteral description) {
}
bool bool_value = false;
const bool &ConfigVarRegistration<bool>::value() {
	return bool_value;
}

// os/os_specific.h
#define utf8printv vsnprintf
const char *floatFormatOld = "%0.4f";
const char *floatFormatNew = "%#.6g";
namespace StringFormat {
rdcstr Fmt(const char *format, ...) {
	va_list args;
	va_start(args, format);

	va_list args2;
	va_copy(args2, args);

	// NOTE: make sure not to lose precision
	if(strcmp(format, floatFormatOld) == 0)
		format = floatFormatNew;

	int size = ::utf8printv(NULL, 0, format, args2);

	rdcstr ret;
	ret.resize(size);
	::utf8printv(ret.data(), size + 1, format, args);

	va_end(args);
	va_end(args2);

	return ret;
}
}

// driver/shaders/dxbc/dxbc_bytecode.h
namespace DXBCBytecode {
void Program::PostprocessVendorExtensions() {
}
}
#if DISABLED(RDOC_X64)
template<> rdcstr DoStringise(unsigned long const& el) {
	return StringFormat::Fmt("%lu", el);
}
#endif