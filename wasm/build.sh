#!/bin/bash

# download
[ ! -d renderdoc ] && curl -L https://github.com/baldurk/renderdoc/archive/refs/tags/v1.26.tar.gz | tar -xz && mv renderdoc-* renderdoc

# compile
$WASI_SDK_PATH/bin/clang -c -O3 -D_WASI_EMULATED_SIGNAL -D__linux__ -Wno-ignored-attributes\
 -I renderdoc/renderdoc -I src/shim\
 renderdoc/renderdoc/serialise/serialiser.cpp\
 renderdoc/renderdoc/strings/string_utils.cpp\
 renderdoc/renderdoc/driver/shaders/dxbc/dxbc_bytecode.cpp\
 renderdoc/renderdoc/driver/shaders/dxbc/dxbc_bytecode_ops.cpp\
 renderdoc/renderdoc/driver/shaders/dxbc/dxbc_stringise.cpp\
 src/main.cpp src/shim.cpp --include src/shim.h

# link
$WASI_SDK_PATH/bin/wasm-ld *.o -o renderdoc.wasm\
 -L $WASI_SDK_PATH/share/wasi-sysroot/lib/wasm32-wasi -lc -lc++abi\
 --no-entry --export=malloc --export=free\
 --export=DXBCBytecode_Program_GetDisassembly --export=strlen

# optimize
wasm-opt -O3 --strip-producers renderdoc.wasm -o renderdoc.min.wasm