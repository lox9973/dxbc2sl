# SPDX-License-Identifier: MIT
# SPDX-FileCopyrightText: © 2023 lox9973
import re
from typing import Iterator

from tr_base import Translator, Template, cast_template_args, extract_template_attributes

class GLSLES3Translator(Translator):
	cast_dynamic_index = "asint" # match offset type
	cast_int_negate = "asint"

	def __init__(self):
		super().__init__()
		self.prelude = prelude
		self.template_func_op = template_func_op
		self.template_ctrl_op = template_ctrl_op
		self.template_op_attrs = template_op_attrs
		self.template_res_src = template_res_src

	def do_declaration(self) -> Iterator[str]:
		if icb := self.dcl_immediateConstantBuffer:
			yield f"const float4 icb[] = float4[]({', '.join(self.do_imm_operand(x) for x in icb)});"
		for name, (size, access) in self.dcl_constantbuffer.items():
			yield f"uniform float4 {name}[{size}];"
		for name, (res_type, ret_type, size) in self.dcl_resource.items():
			yield f"uniform highp {resource_types[res_type+('.cmp' if name in self.state_sample_c else '')]} {name};"
		for name, (mask, sv, interp) in self.dcl_input.items():
			yield f"{'float4' if mask else 'float'} {name};"
		for name, (mask, sv) in self.dcl_output.items():
			yield f"{'float4' if mask else 'float'} {name};"

	def do_header(self) -> Iterator[str]:
		yield f"void {self.shader_stage}_main() {{"
		yield "const int MAX_LOOP = 64;"
		if self.dcl_temps:
			yield "float4 %s;" % ", ".join(f"r{i}" for i in range(self.dcl_temps))
		for name, (size, comp) in self.dcl_indexableTemp.items():
			yield f"float{comp} {name}[{size}];"

	def do_footer(self) -> Iterator[str]:
		yield "}"
		yield ""

		input_decls = []
		input_assigns = []
		for name, (mask, sv, interp) in self.dcl_input.items():
			if info := input_types.get(f"{sv or name}.{self.shader_stage}") or input_types.get(sv or name):
				sv, type = info
				if sv.startswith("layout"):
					vname = f"in_{name}"
					input_decls.append(f"{interp_modifiers[interp] if interp else ''}{sv} in {type} {vname};")
				else:
					vname = f"int({sv})" if type == "bool" else f"{sv}"
				input_assigns.append(f"{name} = asfloat({vname});")

		output_decls = []
		output_assigns = []
		for name, (mask, sv) in self.dcl_output.items():
			if info := output_types.get(f"{sv or name}.{self.shader_stage}") or output_types.get(sv or name):
				sv, type = info
				if sv.startswith("layout"):
					oname = f"out_{name}"
					output_decls.append(f"{sv} out {type} {oname};")
				else:
					oname = sv
				output_assigns.append(f"{oname} = as{type.rstrip('4')}({name});")

		yield from input_decls
		yield from output_decls

		yield "void main() {"
		yield from input_assigns
		yield f"{self.shader_stage}_main();"
		yield from output_assigns
		yield "}"
	
prelude = [
	# polyfill hlsl
	"#define saturate(x) clamp((x), 0., 1.)",
	*sum(([
		f"#define int{n} ivec{n}",
		f"#define uint{n} uvec{n}",
		f"#define float{n} vec{n}",
		f"#define bool{n} bvec{n}",
	] for n in [2,3,4]), []),
	*sum(([
		f"int{n} asint(int{n} x) {{ return x; }}",
		f"int{n} asint(uint{n} x) {{ return int{n}(x); }}",
		f"int{n} asint(float{n} x) {{ return floatBitsToInt(x); }}",
		f"uint{n} asuint(int{n} x) {{ return uint{n}(x); }}",
		f"uint{n} asuint(uint{n} x) {{ return x; }}",
		f"uint{n} asuint(float{n} x) {{ return floatBitsToUint(x); }}",
		f"float{n} asfloat(int{n} x) {{ return intBitsToFloat(x); }}",
		f"float{n} asfloat(uint{n} x) {{ return uintBitsToFloat(x); }}",
		f"float{n} asfloat(float{n} x) {{ return x; }}",
	] for n in ["","2","3","4"]), []),
	"float  f16tof32(uint e)  { return unpackHalf2x16(e).x; }",
	"float2 f16tof32(uint2 e) { return float2(f16tof32(e.x), f16tof32(e.y)); }",
	"float3 f16tof32(uint3 e) { return float3(f16tof32(e.x), f16tof32(e.y), f16tof32(e.z)); }",
	"float4 f16tof32(uint4 e) { return float4(f16tof32(e.x), f16tof32(e.y), f16tof32(e.z), f16tof32(e.w)); }",
	"uint  f32tof16(float e)  { return packHalf2x16(float2(e, 0)); }",
	"uint2 f32tof16(float2 e) { return uint2(f32tof16(e.x), f32tof16(e.y)); }",
	"uint3 f32tof16(float3 e) { return uint3(f32tof16(e.x), f32tof16(e.y), f32tof16(e.z)); }",
	"uint4 f32tof16(float4 e) { return uint4(f32tof16(e.x), f32tof16(e.y), f32tof16(e.z), f32tof16(e.w)); }",
	# fix scalar comparison
	*sum(([
		f"#define {name}_bool(x, y) ((x) {op} (y))",
		f"#define {name}_bool2 {name}",
		f"#define {name}_bool3 {name}",
		f"#define {name}_bool4 {name}",
	] for name, op in [("equal", "=="), ("greaterThanEqual", ">="), ("lessThan", "<"), ("notEqual", "!=")]), []),
	# concat floats. used by sample_c
	"float3 vec(float2 x, float y) { return float3(x, y); }",
	"float4 vec(float3 x, float y) { return float4(x, y); }",
	# polyfill: bit
	"#if !(__VERSION__ >= 310)",
		"#define bitfieldReverse(x) (x)", # HACK
		"#define bitCount(x) ((x)-(x))",  # HACK
		"#define findMSB(x)  ((x)-(x))",  # HACK
		"#define findLSB(x)  ((x)-(x))",  # HACK
		"#define findMSB(x)  ((x)-(x))",  # HACK
	# polyfill: resource
		"#define sampler2DMS sampler2D",
		"#define texelFetch_ms(s, p, S) texelFetch(s, p, 0)",
		"#define LOD_ms ,0",
		"#define textureGather(s, p, c) float4(texture(s, p)[c])",
		"#define textureGather_c(s, p, r) float4(texture(s, vec(p, r)))",
		"#define textureGatherOffset(s, p, o, c) textureGather(s, p, c)",
		"#define textureGatherOffset_c(s, p, r, o) textureGather_c(s, p, r)",
	"#else",
		"#define texelFetch_ms texelFetch",
		"#define LOD_ms",
		"#define textureGather_c textureGather",
		"#define textureGatherOffset_c textureGatherOffset",
	"#endif",
	# fallback: unsupported lod/offset on sampler2DArrayShadow/sampler2DMS
	"#define textureLod_c(s, p, l) texture(s, p)",
	"#define textureOffset_c(s, p, o) texture(s, p)",
	"#define textureLodOffset_c(s, p, l, o) textureLod_c(s, p, l)",
	"#define texelFetchOffset_ms(s, p, S, o) texelFetch_ms(s, p, S)",
	# polyfill: resource info and sample
	"#if !(__VERSION__ >= 320)",
		"#define sample",
		"#define gl_SampleID int(0)", # TODO: gl_SampleMask, gl_SampleMaskIn
		"#define gl_NumSamples int(1)",
		"#define gl_SamplePosition float2(0.5, 0.5)",
		"#define gl_PrimitiveID int(0)",
	"#endif",
	"#if !(defined(GL_EXT_texture_query_lod))",
		"#define textureQueryLod(s, p) float2(log2(length(fwidth(p)))-0.5)", # GLSL 4.0
	"#endif",
	"#define textureQueryLevels(s) int(1)", # GLSL 4.3
	"#define textureSamples(s) int(1)", # GLSL 4.5
]
template_func_op: dict[str, Template] = {
	# mov
	"mov":  "asfloat({0})",
	"movc": "mix(asfloat({2}), asfloat({1}), boolN(asuint({0})))",
	"mov.float":  "{0}",
	"movc.float": "mix({2}, {1}, boolN(asuint({0})))",

	# cast
	"f16tof32": "f16tof32(asuint({0}))",
	"f32tof16": "asfloat(f32tof16({0}))",
	"ftoi": "asfloat(intN({0}))",
	"ftou": "asfloat(uintN({0}))",
	"itof": "floatN(asint({0}))",
	"utof": "floatN(asuint({0}))",

	# floatN => floatN
	"deriv_rtx":        "dFdx({0})",
	"deriv_rtx_coarse": "dFdx({0})",
	"deriv_rtx_fine":   "dFdx({0})",
	"deriv_rty":        "dFdy({0})",
	"deriv_rty_coarse": "dFdy({0})",
	"deriv_rty_fine":   "dFdy({0})",
	"exp":      "exp2({0})",
	"frc":      "fract({0})",
	"log":      "log2({0})",
	"rcp":      "(1. / {0})",
	"round_ne": "round({0})",
	"round_ni": "floor({0})",
	"round_pi": "ceil({0})",
	"round_z":  "trunc({0})",
	"rsq":      "inversesqrt({0})",
	"sincos.0": "sin({0})",
	"sincos.1": "cos({0})",
	"sqrt":     "sqrt({0})",
	# floatN => float
	"dp2": "dot({0}.xy, {1}.xy).rrrr",
	"dp3": "dot({0}.xyz, {1}.xyz).rrrr",
	"dp4": "dot({0}.xyzw, {1}.xyzw).rrrr",

	**dict(cast_template_args({
		# TN => TN
		"add": "({0} + {1})",
		"div": "({0} / {1})",
		"div.0": "({0} / {1})",
		"div.1": "({0} % {1})",
		"mad": "({0} * {1} + {2})",
		"max": "max({0}, {1})",
		"min": "min({0}, {1})",
		"mul": "({0} * {1})",
		"mul.0": "({0} - {0})", # HACK
		"mul.1": "({0} * {1})",
		# TN => boolN
		"eq": "asfloat(uintN(equal_boolN({0}, {1})) * ~uint(0))",
		"ge": "asfloat(uintN(greaterThanEqual_boolN({0}, {1})) * ~uint(0))",
		"lt": "asfloat(uintN(lessThan_boolN({0}, {1})) * ~uint(0))",
		"ne": "asfloat(uintN(notEqual_boolN({0}, {1})) * ~uint(0))",
	}, [("", "", ""), ("u", "asuint", "asfloat"), ("i", "asint", "asfloat")])),

	# intN => intN
	"bfrev":        "asfloat(bitfieldReverse(asuint({0})))",
	"countbits":    "asfloat(bitCount(asuint({0})))",
	"firstbit_hi":  "asfloat(findMSB(asuint({0})))",
	"firstbit_lo":  "asfloat(findLSB(asuint({0})))",
	"firstbit_shi": "asfloat(findMSB(asint({0})))",
	"ineg":   "asfloat(-asint({0}))",
	"not":    "asfloat(~asuint({0}))",
	"and":    "asfloat(asuint({0}) & asuint({1}))",
	"ishl":   "asfloat(asint({0}) << asuint({1}))",
	"ishr":   "asfloat(asint({0}) >> asuint({1}))",
	"or":     "asfloat(asuint({0}) | asuint({1}))",
	"ushr":   "asfloat(asuint({0}) >> asuint({1}))",
	"xor":    "asfloat(asuint({0}) ^ asuint({1}))",

	# resource
	"ld":          "texelFetch({1.resource}, {0.address_ld}).xyzw",
	"ldms":        "texelFetch_ms({1.resource}, {0.address_ld}, asint({2})).xyzw",
	"sample":      "texture({1.resource}, {0.address}).xyzw",
	"sample_b":    "texture({1.resource}, {0.address}, {3}).xyzw",
	"sample_d":    "textureGrad({1.resource}, {0.address}, {3.ddx}, {4.ddy}).xyzw",
	"sample_l":    "textureLod({1.resource}, {0.address}, {3}).xyzw",
	"sample_c":    "texture({1.resource}, vec({0.address}, {3})).rrrr",
	"sample_c_lz": "textureLod_c({1.resource}, vec({0.address}, {3}), 0.).rrrr",
	"gather4.x":   "textureGather({1.resource}, {0.address}, 0).xyzw",
	"gather4.y":   "textureGather({1.resource}, {0.address}, 1).xyzw",
	"gather4.z":   "textureGather({1.resource}, {0.address}, 2).xyzw",
	"gather4.w":   "textureGather({1.resource}, {0.address}, 3).xyzw",
	"gather4_c":   "textureGather_c({1.resource}, {0.address}, {3}).xyzw",
	"gather4_po.x":   "textureGatherOffset({2.resource}, {0.address}, asint({1.offset}), 0).xyzw",
	"gather4_po.y":   "textureGatherOffset({2.resource}, {0.address}, asint({1.offset}), 1).xyzw",
	"gather4_po.z":   "textureGatherOffset({2.resource}, {0.address}, asint({1.offset}), 2).xyzw",
	"gather4_po.w":   "textureGatherOffset({2.resource}, {0.address}, asint({1.offset}), 3).xyzw",
	"gather4_po_c":   "textureGatherOffset_c({2.resource}, {0.address}, {4}, asint({1.offset})).xyzw",
	"lod": "float4(textureQueryLod({1.resource}, {0.ddx}), 0, 0).xyzw",

	# resource aoffimmi
	"ld_aoffimmi":          "texelFetchOffset({1.resource}, {0.address_ld}, {2.offset}).xyzw",
	"ldms_aoffimmi":        "texelFetchOffset_ms({1.resource}, {0.address_ld}, asint({2}), {3.offset}).xyzw",
	"sample_aoffimmi":      "textureOffset({1.resource}, {0.address}, {3.offset}).xyzw",
	"sample_b_aoffimmi":    "textureOffset({1.resource}, {0.address}, {4.offset}, {3}).xyzw",
	"sample_d_aoffimmi":    "textureGradOffset({1.resource}, {0.address}, {3.ddx}, {4.ddy}, {5.offset}).xyzw",
	"sample_l_aoffimmi":    "textureLodOffset({1.resource}, {0.address}, {3}, {4.offset}).xyzw",
	"sample_c_aoffimmi":    "textureOffset_c({1.resource}, vec({0.address}, {3}), {4.offset}).rrrr",
	"sample_c_lz_aoffimmi": "textureLodOffset_c({1.resource}, vec({0.address}, {3}), 0., {4.offset}).rrrr",
	"gather4_aoffimmi.x":   "textureGatherOffset({1.resource}, {0.address}, {3.offset}, 0).xyzw",
	"gather4_aoffimmi.y":   "textureGatherOffset({1.resource}, {0.address}, {3.offset}, 1).xyzw",
	"gather4_aoffimmi.z":   "textureGatherOffset({1.resource}, {0.address}, {3.offset}, 2).xyzw",
	"gather4_aoffimmi.w":   "textureGatherOffset({1.resource}, {0.address}, {3.offset}, 3).xyzw",
	"gather4_c_aoffimmi":   "textureGatherOffset_c({1.resource}, {0.address}, {3}, {4.offset}).xyzw",

	# resource info
	"resinfo":         "float4(int4(textureSize({1.resource}{0.resinfo0}))).xyzw",
	"resinfo_uint":    "asfloat(int4(textureSize({1.resource}{0.resinfo0}))).xyzw",
	"sampleinfo":      "float4(float(textureSamples({0.resource})), 0, 0, 0).xyzw",
	"sampleinfo_uint": "float4(asfloat(textureSamples({0.resource})), 0, 0, 0).xyzw",
	"sampleinfo.rasterizer":      "float4(float(gl_NumSamples), 0, 0, 0).xyzw",
	"sampleinfo_uint.rasterizer": "float4(asfloat(gl_NumSamples), 0, 0, 0).xyzw",
	"samplepos.rasterizer":       "float4(gl_SamplePosition-0.5, 0, 0).xyzw",
	"samplepos":                  "float4(0, 0, 0, 0).xyzw", # fallback
}
template_op_attrs = extract_template_attributes(template_func_op)

template_ctrl_op: dict[str, Template] = {
	"nop": "",

	"loop": "{{for (int I=0; I<MAX_LOOP; I++) {{",
	"endloop": "}}}}",

	"if_z": "if (!bool(asuint({0}))) {{",
	"if_nz": "if (bool(asuint({0}))) {{",
	"else": "}} else {{",
	"endif": "}}",

	"switch": "switch (asint({0})) {{", # match int case
	"case": "case {0}:",
	"default": "default:",
	"endswitch": "}}",
}
# add break etc
for opc, st in [("breakc","break;"), ("continuec","continue;"), ("retc","return;"), ("discard","discard;")]:
	template_ctrl_op.update({
		opc.rstrip('c'): st,
		f"{opc}_z": "if (!bool(asuint({0}))) {{ "+st+" }}",
		f"{opc}_nz": "if (bool(asuint({0}))) {{ "+st+" }}",
	})

template_res_src: dict[str, dict[str, Template]] = {
	# https://registry.khronos.org/OpenGL-Refpages/es3/html/texelFetch.xhtml
	"address_ld": {
		"texture2d":        "asint({0}.xy), asint({0}.w)",
		"texture2darray":   "asint({0}.xyz), asint({0}.w)",
		"texture2dms":      "asint({0}.xy)",
		"texture3d":        "asint({0}.xyz), asint({0}.w)",
	},
	# https://registry.khronos.org/OpenGL-Refpages/es3/html/texture.xhtml
	"address": {
		"texture2d":        "{0}.xy",
		"texture2darray":   "{0}.xyz",
		"texture3d":        "{0}.xyz",
		"texturecube":      "{0}.xyz",
	},
	# https://registry.khronos.org/OpenGL-Refpages/es3/html/texelFetchOffset.xhtml
	"offset": { # asint() is done at caller to make aoffimmi constexpr
		"texture2d":        "{0}.xy",
		"texture2darray":   "{0}.xy",
		"texture2dms":      "{0}.xy",
		"texture3d":        "{0}.xyz",
	},
	# https://registry.khronos.org/OpenGL-Refpages/es3/html/textureGrad.xhtml
	**{k: {
		"texture2d":        "{0}.xy",
		"texture2darray":   "{0}.xy",
		"texture3d":        "{0}.xyz",
		"texturecube":      "{0}.xyz",
	} for k in ["ddx", "ddy"]},
	# https://registry.khronos.org/OpenGL-Refpages/es3/html/textureSize.xhtml
	"resinfo0": {
		"texture2d":        ", asint({0})), 0, textureQueryLevels({1}",
		"texture2darray":   ", asint({0})), textureQueryLevels({1}",
		"texture2dms":      " LOD_ms), 0, (1",
		"texture3d":        ", asint({0})), textureQueryLevels({1}",
		"texturecube":      ", asint({0})), 0, textureQueryLevels({1}",
	},
}
# https://registry.khronos.org/OpenGL-Refpages/es3/html/texture.xhtml
resource_types = {
	"texture2d":        "sampler2D",
	"texture2darray":   "sampler2DArray",
	"texture2dms":      "sampler2DMS",
	"texture3d":        "sampler3D",
	"texturecube":      "samplerCube",
	"texture2d.cmp":        "sampler2DShadow",
	"texture2darray.cmp":   "sampler2DArrayShadow",
	"texturecube.cmp":      "samplerCubeShadow",
}
# https://www.khronos.org/opengl/wiki/Type_Qualifier_(GLSL)
interp_modifiers = {
	"constant":        "flat ",
	"linear":          "",
	"linear centroid": "centroid ",
	"linear sample":   "sample ",
	"linear noperspective":          "", # not available in GLES
	"linear noperspective centroid": "centroid ",
	"linear noperspective sample":   "sample ",
}
# https://www.khronos.org/opengl/wiki/Built-in_Variable_(GLSL)
input_types = {
	**{f"v{i}":      (f"layout(location = {i})", "float4") for i in range(16)},
	"position":      ("gl_FragCoord", "float4"),
	"vertexid":      ("gl_VertexID", "int"), 
	"instanceid":    ("gl_InstanceID", "int"),
	"isfrontface":   ("gl_FrontFacing", "bool"),
	"sampleidx":     ("gl_SampleID", "int"),
	"vCoverageMask": ("gl_SampleMaskIn[0]", "int"),
	"vPrim":         ("gl_PrimitiveID", "int"),
}
output_types = {
	**{f"o{i}":      (f"layout(location = {i})", "float4") for i in range(16)},
	"position":      ("gl_Position", "float4"),
	"oMask":         ("gl_SampleMask[0]", "int"),
	"oDepth":        ("gl_FragDepth", "float"),
	"oDepthGreaterEqual": ("gl_FragDepth", "float"), # TODO: layout(depth_greater) out float gl_FragDepth
	"oDepthLessEqual":    ("gl_FragDepth", "float"), # TODO: layout(depth_less) out float gl_FragDepth
}