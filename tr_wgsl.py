# SPDX-License-Identifier: MIT
# SPDX-FileCopyrightText: © 2023 lox9973
import re
from typing import Iterator

from tr_base import Translator, Template, cast_template_args, extract_template_attributes

class WGSLTranslator(Translator):
	cast_dynamic_index = "bitcast<uint>"
	cast_int_negate = "bitcast<intN>"

	def __init__(self):
		super().__init__()
		self.prelude = prelude
		self.template_func_op = template_func_op
		self.template_ctrl_op = template_ctrl_op
		self.template_op_attrs = template_op_attrs
		self.template_res_src = template_res_src

	@classmethod
	def do_assign(self, target: str, source: str, free_mask: bool=True) -> str:
		return super().do_assign(target, source, free_mask=False)

	def do_declaration(self) -> Iterator[str]:
		if icb := self.dcl_immediateConstantBuffer:
			yield f"const icb = array({', '.join(self.do_imm_operand(x) for x in icb)});"
		for name, (size, access) in self.dcl_constantbuffer.items():
			yield f"var<uniform> {name}: array<float4, {size}>;"
		for name, (res_type, ret_type, size) in self.dcl_resource.items():
			yield f"var {name}: {resource_types[res_type+('.cmp' if name in self.state_sample_c else '')]};"
		for name, (mode,) in self.dcl_sampler.items():
			yield f"var {name}: {sampler_types[mode]};"
		for name, (mask, sv, interp) in self.dcl_input.items():
			yield f"var<private> {name}: {'float4' if mask else 'float'};"
		for name, (mask, sv) in self.dcl_output.items():
			yield f"var<private> {name}: {'float4' if mask else 'float'};"

	def do_header(self) -> Iterator[str]:
		yield f"fn {self.shader_stage}_main() {{"
		yield "const MAX_LOOP = 64;"
		if self.dcl_temps:
			yield " ".join(f"var r{i}: float4;" for i in range(self.dcl_temps))
		for name, (size, comp) in self.dcl_indexableTemp.items():
			yield f"var {name}: array<float{comp}, {size}>;"

	def do_footer(self) -> Iterator[str]:
		yield "}"
		yield ""

		input_decls = []
		input_assigns = []
		for name, (mask, sv, interp) in self.dcl_input.items():
			if info := input_types.get(f"{sv or name}.{self.shader_stage}") or input_types.get(sv or name):
				sv, type = info
				vname = f"int(v.{name})" if type == "bool" else f"v.{name}"
				input_decls.append(f"{sv} {interp_modifiers[interp] if interp else ''}{name}: {type},")
				input_assigns.append(f"{name} = bitcast<{'float4' if mask else 'float'}>({vname});")

		output_decls = []
		output_assigns = []
		for name, (mask, sv) in self.dcl_output.items():
			if info := output_types.get(f"{sv or name}.{self.shader_stage}") or output_types.get(sv or name):
				sv, type = info
				output_decls.append(f"{sv} {name}: {type},")
				output_assigns.append(f"o.{name} = bitcast<{type}>({name});")

		if input_decls:
			yield f"struct {self.shader_stage}_input {{"
			yield from input_decls
			yield "}"
		if output_decls:
			yield f"struct {self.shader_stage}_output {{"
			yield from output_decls
			yield "}"

		yield f"@{stage_names[self.shader_stage]}"
		yield f"fn main({f'v: {self.shader_stage}_input' if input_decls else ''}){f' -> {self.shader_stage}_output' if output_decls else ''} {{"
		yield from input_assigns
		yield f"{self.shader_stage}_main();"
		if output_decls:
			yield f"var o: {self.shader_stage}_output;"
		yield from output_assigns
		if output_decls:
			yield "return o;"
		yield "}"

prelude = [
	# polyfill hlsl
	"alias int  =   i32; alias uint  =   u32; alias float  =   f32;",
	"alias int2 = vec2i; alias uint2 = vec2u; alias float2 = vec2f; alias bool2 = vec2<bool>;",
	"alias int3 = vec3i; alias uint3 = vec3u; alias float3 = vec3f; alias bool3 = vec3<bool>;",
	"alias int4 = vec4i; alias uint4 = vec4u; alias float4 = vec4f; alias bool4 = vec4<bool>;",
	"fn f16tof32_float(e: uint)   -> float  { return unpack2x16float(e).x; }",
	"fn f16tof32_float2(e: uint2) -> float2 { return float2(f16tof32_float(e.x), f16tof32_float(e.y)); }",
	"fn f16tof32_float3(e: uint3) -> float3 { return float3(f16tof32_float(e.x), f16tof32_float(e.y), f16tof32_float(e.z)); }",
	"fn f16tof32_float4(e: uint4) -> float4 { return float4(f16tof32_float(e.x), f16tof32_float(e.y), f16tof32_float(e.z), f16tof32_float(e.w)); }",
	"fn f32tof16_uint(e: float)   -> uint  { return pack2x16float(float2(e, 0)); }",
	"fn f32tof16_uint2(e: float2) -> uint2 { return uint2(f32tof16_uint(e.x), f32tof16_uint(e.y)); }",
	"fn f32tof16_uint3(e: float3) -> uint3 { return uint3(f32tof16_uint(e.x), f32tof16_uint(e.y), f32tof16_uint(e.z)); }",
	"fn f32tof16_uint4(e: float4) -> uint4 { return uint4(f32tof16_uint(e.x), f32tof16_uint(e.y), f32tof16_uint(e.z), f32tof16_uint(e.w)); }",
]
template_func_op: dict[str, Template] = {
	# mov
	"mov":  "bitcast<floatN>({0})", # TODO: cast constant to float might trigger nan error
	"movc": "bitcast<floatN>(select(bitcast<uintN>({2}), bitcast<uintN>({1}), boolN(bitcast<uintN>({0}))))",
	"mov.float":  "{0}",
	"movc.float": "select({2}, {1}, boolN(bitcast<uintN>({0})))",

	# cast
	"f16tof32": "f16tof32_floatN(bitcast<uintN>({0}))",
	"f32tof16": "bitcast<floatN>(f32tof16_uintN({0}))",
	"ftoi": "bitcast<floatN>(intN({0})*3*-0x55555555)", # HACK: suppress opt
	"ftou": "bitcast<floatN>(uintN({0})*3*0xaaaaaaab)", # HACK: suppress opt
	"itof": "floatN(bitcast<intN>({0}))",
	"utof": "floatN(bitcast<uintN>({0}))",

	# floatN => floatN
	"deriv_rtx":        "dpdx({0})",
	"deriv_rtx_coarse": "dpdxCoarse({0})",
	"deriv_rtx_fine":   "dpdxFine({0})",
	"deriv_rty":        "dpdy({0})",
	"deriv_rty_coarse": "dpdyCoarse({0})",
	"deriv_rty_fine":   "dpdyFine({0})",
	"exp":      "exp2({0})",
	"frc":      "fract({0})",
	"log":      "log2({0})",
	"rcp":      "(1. / {0})",
	"round_ne": "round({0})",
	"round_ni": "floor({0})",
	"round_pi": "ceil({0})",
	"round_z":  "trunc({0})",
	"rsq":      "inverseSqrt({0})",
	"sincos.0": "sin({0})",
	"sincos.1": "cos({0})",
	"sqrt":     "sqrt({0})",
	# floatN => float
	"dp2": "dot({0}.xy, {1}.xy).rrrr",
	"dp3": "dot({0}.xyz, {1}.xyz).rrrr",
	"dp4": "dot({0}.xyzw, {1}.xyzw).rrrr",

	**dict(cast_template_args({
		# TN => TN
		"add": "({0} + {1})",
		"div": "({0} / {1})",
		"div.0": "({0} / {1})",
		"div.1": "({0} % {1})",
		"mad": "({0} * {1} + {2})",
		"max": "max({0}, {1})",
		"min": "min({0}, {1})",
		"mul": "({0} * {1})",
		"mul.0": "({0} - {0})", # HACK
		"mul.1": "({0} * {1})",
		# TN => boolN
		"eq": "bitcast<floatN>(select(uintN(0), ~uintN(0), {0} == {1}))",
		"ge": "bitcast<floatN>(select(uintN(0), ~uintN(0), {0} >= {1}))",
		"lt": "bitcast<floatN>(select(uintN(0), ~uintN(0), {0} <  {1}))",
		"ne": "bitcast<floatN>(select(uintN(0), ~uintN(0), {0} != {1}))",
	}, [("", "", ""), ("u", "bitcast<uintN>", "bitcast<floatN>"), ("i", "bitcast<intN>", "bitcast<floatN>")])),
	"mad": "fma({0}, {1}, {2})",

	# intN => intN
	"bfrev":        "bitcast<floatN>(reverseBits(bitcast<uintN>({0})))",
	"countbits":    "bitcast<floatN>(countOneBits(bitcast<uintN>({0})))",
	"firstbit_hi":  "bitcast<floatN>(firstLeadingBit(bitcast<uintN>({0})))",
	"firstbit_lo":  "bitcast<floatN>(firstTrailingBit(bitcast<uintN>({0})))",
	"firstbit_shi": "bitcast<floatN>(firstLeadingBit(bitcast<intN>({0})))",
	"ineg":   "bitcast<floatN>(-bitcast<intN>({0}))",
	"not":    "bitcast<floatN>(~bitcast<uintN>({0}))",
	"and":    "bitcast<floatN>(bitcast<uintN>({0}) & bitcast<uintN>({1}))",
	"ishl":   "bitcast<floatN>(bitcast<intN>({0}) << bitcast<uintN>({1}))",
	"ishr":   "bitcast<floatN>(bitcast<intN>({0}) >> bitcast<uintN>({1}))",
	"or":     "bitcast<floatN>(bitcast<uintN>({0}) | bitcast<uintN>({1}))",
	"ushr":   "bitcast<floatN>(bitcast<uintN>({0}) >> bitcast<uintN>({1}))",
	"xor":    "bitcast<floatN>(bitcast<uintN>({0}) ^ bitcast<uintN>({1}))",
	"bfi":  "bitcast<floatN>(insertBits(bitcast<uintN>({2}), bitcast<uintN>({3}), bitcast<uint>({1}), bitcast<uint>({0})))",
	"ibfe": "bitcast<floatN>(extractBits(bitcast<intN>({2}), bitcast<uint>({1}), bitcast<uint>({0})))",
	"ubfe": "bitcast<floatN>(extractBits(bitcast<uintN>({2}), bitcast<uint>({1}), bitcast<uint>({0})))",

	# resource
	"ld":          "textureLoad({1.resource}, {0.address_ld}).xyzw",
	"ldms":        "textureLoad({1.resource}, {0.address_ld}, bitcast<int>({2})).xyzw",
	"sample":      "textureSample({1.resource}, {2}, {0.address}).xyzw",
	"sample_b":    "textureSampleBias({1.resource}, {2}, {0.address}, {3}).xyzw",
	"sample_d":    "textureSampleGrad({1.resource}, {2}, {0.address}, {3.ddx}, {4.ddy}).xyzw",
	"sample_l":    "textureSampleLevel({1.resource}, {2}, {0.address}, {3}).xyzw",
	"sample_c":    "textureSampleCompare({1.resource}, {2}, {0.address}, {3}).rrrr",
	"sample_c_lz": "textureSampleCompareLevel({1.resource}, {2}, {0.address}, {3}).rrrr",
	"gather4.x":   "textureGather(0, {1.resource}, {2}, {0.address}).xyzw",
	"gather4.y":   "textureGather(1, {1.resource}, {2}, {0.address}).xyzw",
	"gather4.z":   "textureGather(2, {1.resource}, {2}, {0.address}).xyzw",
	"gather4.w":   "textureGather(3, {1.resource}, {2}, {0.address}).xyzw",
	"gather4_c":   "textureGatherCompare({1.resource}, {2}, {0.address}, {3}).xyzw",
	"gather4_po.x":   "textureGather(0, {2.resource}, {3}, {0.address}/*, {1.offset}*/).xyzw", # fallback
	"gather4_po.y":   "textureGather(1, {2.resource}, {3}, {0.address}/*, {1.offset}*/).xyzw", # fallback
	"gather4_po.z":   "textureGather(2, {2.resource}, {3}, {0.address}/*, {1.offset}*/).xyzw", # fallback
	"gather4_po.w":   "textureGather(3, {2.resource}, {3}, {0.address}/*, {1.offset}*/).xyzw", # fallback
	"gather4_po_c":   "textureGatherCompare({2.resource}, {3}, {0.address}, {4}/*, {1.offset}*/).xyzw", # fallback
	"lod": "float4(/*{1.resource}*/float2(log2(length(fwidth({0.ddx})))-0.5), 0, 0).xyzw", # fallback

	# resource aoffimmi
	"ld_aoffimmi":          "textureLoad({1.resource}, {2.offset} + {0.address_ld}).xyzw",
	"ldms_aoffimmi":        "textureLoad({1.resource}, {3.offset} + {0.address_ld}, bitcast<int>({2})).xyzw",
	"sample_aoffimmi":      "textureSample({1.resource}, {2}, {0.address}, {3.offset}).xyzw",
	"sample_b_aoffimmi":    "textureSampleBias({1.resource}, {2}, {0.address}, {3}, {4.offset}).xyzw",
	"sample_d_aoffimmi":    "textureSampleGrad({1.resource}, {2}, {0.address}, {3.ddx}, {4.ddy}, {5.offset}).xyzw",
	"sample_l_aoffimmi":    "textureSampleLevel({1.resource}, {2}, {0.address}, {3}, {4.offset}).xyzw",
	"sample_c_aoffimmi":    "textureSampleCompare({1.resource}, {2}, {0.address}, {3}, {4.offset}).rrrr",
	"sample_c_lz_aoffimmi": "textureSampleCompareLevel({1.resource}, {2}, {0.address}, {3}, {4.offset}).rrrr",
	"gather4_aoffimmi.x":   "textureGather(0, {1.resource}, {2}, {0.address}, {3.offset}).xyzw",
	"gather4_aoffimmi.y":   "textureGather(1, {1.resource}, {2}, {0.address}, {3.offset}).xyzw",
	"gather4_aoffimmi.z":   "textureGather(2, {1.resource}, {2}, {0.address}, {3.offset}).xyzw",
	"gather4_aoffimmi.w":   "textureGather(3, {1.resource}, {2}, {0.address}, {3.offset}).xyzw",
	"gather4_c_aoffimmi":   "textureGatherCompare({1.resource}, {2}, {0.address}, {3}, {4.offset}).xyzw",

	# resource info
	"resinfo":         "float4(uint4(textureDimensions({1.resource}{0.resinfo0}))).xyzw",
	"resinfo_uint":    "bitcast<float4>(uint4(textureDimensions({1.resource}{0.resinfo0}))).xyzw",
	"sampleinfo":      "float4(float(textureNumSamples({0.resource})), 0, 0, 0).xyzw",
	"sampleinfo_uint": "float4(bitcast<float>(textureNumSamples({0.resource})), 0, 0, 0).xyzw",
	"sampleinfo.rasterizer":      "float4(float(uint(1)), 0, 0, 0).xyzw", # fallback
	"sampleinfo_uint.rasterizer": "float4(bitcast<float>(uint(1)), 0, 0, 0).xyzw", # fallback
	"samplepos.rasterizer":       "float4(0, 0, 0, 0).xyzw", # fallback
	"samplepos":                  "float4(0, 0, 0, 0).xyzw", # fallback
}
template_op_attrs = extract_template_attributes(template_func_op)

template_ctrl_op: dict[str, Template] = {
	"nop": "",

	"loop": "{{for (var I=0; I<MAX_LOOP; I++) {{",
	"endloop": "}}}}",

	"if_z": "if !bool(bitcast<uint>({0})) {{",
	"if_nz": "if bool(bitcast<uint>({0})) {{",
	"else": "}} else {{",
	"endif": "}}",

	"switch": "switch bitcast<uint>({0}) {{",
	"case": "case {0},",
	"case.fallthrough": "{0},",
	"default": "default",
	"endswitch": "}}",
}
# add break etc
for opc, st in [("breakc","break;"), ("continuec","continue;"), ("retc","return;"), ("discard","discard;")]:
	template_ctrl_op.update({
		opc.rstrip('c'): st,
		f"{opc}_z": "if !bool(bitcast<uint>({0})) {{ "+st+" }}",
		f"{opc}_nz": "if bool(bitcast<uint>({0})) {{ "+st+" }}",
	})

template_res_src: dict[str, dict[str, Template]] = {
	# https://www.w3.org/TR/WGSL/#textureload
	"address_ld": {
		"texture1d":        "bitcast<int>({0}.x), bitcast<int>({0}.w)",
		"texture2d":        "bitcast<int2>({0}.xy), bitcast<int>({0}.w)",
		"texture2darray":   "bitcast<int2>({0}.xy), bitcast<int>({0}.z), bitcast<int>({0}.w)",
		"texture2dms":      "bitcast<int2>({0}.xy)",
		"texture3d":        "bitcast<int3>({0}.xyz), bitcast<int>({0}.w)",
	},
	# https://www.w3.org/TR/WGSL/#texturesample
	"address": {
		"texture1d":        "{0}.x",
		"texture2d":        "{0}.xy",
		"texture2darray":   "{0}.xy, int({0}.z)",
		"texture3d":        "{0}.xyz",
		"texturecube":      "{0}.xyz",
		"texturecubearray": "{0}.xyz, int({0}.w)",
	},
	# https://www.w3.org/TR/WGSL/#texturesample
	"offset": {
		"texture1d":        "bitcast<int>({0}.x)", # not supported by textureSample
		"texture2d":        "bitcast<int2>({0}.xy)",
		"texture2darray":   "bitcast<int2>({0}.xy)",
		"texture2dms":      "bitcast<int2>({0}.xy)",
		"texture3d":        "bitcast<int3>({0}.xyz)",
	},
	# https://www.w3.org/TR/WGSL/#texturesamplegrad
	**{k: {
		"texture2d":        "{0}.xy",
		"texture2darray":   "{0}.xy",
		"texture3d":        "{0}.xyz",
		"texturecube":      "{0}.xyz",
		"texturecubearray": "{0}.xyz",
	} for k in ["ddx", "ddy"]},
	# https://www.w3.org/TR/WGSL/#texturenumlayers
	"resinfo0": {
		"texture1d":        ", bitcast<uint>({0})), 0, 0, textureNumLevels({1}",
		"texture2d":        ", bitcast<uint>({0})), 0, textureNumLevels({1}",
		"texture2darray":   ", bitcast<uint>({0})), textureNumLayers({1}), textureNumLevels({1}",
		"texture2dms":      "), 0, (1",
		"texture3d":        ", bitcast<uint>({0})), textureNumLevels({1}",
		"texturecube":      ", bitcast<uint>({0})), 0, textureNumLevels({1}",
		"texturecubearray": ", bitcast<uint>({0})), textureNumLayers({1}), textureNumLevels({1}",
	},
}
# https://www.w3.org/TR/WGSL/#sampled-texture-type
resource_types = {
	"texture1d":        "texture_1d<float>",
	"texture2d":        "texture_2d<float>",
	"texture2darray":   "texture_2d_array<float>",
	"texture2dms":      "texture_multisampled_2d<float>",
	"texture3d":        "texture_3d<float>",
	"texturecube":      "texture_cube<float>",
	"texturecubearray": "texture_cube_array<float>",
	"texture2d.cmp":        "texture_depth_2d",
	"texture2darray.cmp":   "texture_depth_2d_array",
	"texturecube.cmp":      "texture_depth_cube",
	"texturecubearray.cmp": "texture_depth_cube_array",
}
# https://www.w3.org/TR/WGSL/#sampler-type
sampler_types = {
	"default": "sampler",
	"comparison": "sampler_comparison",
}
# https://www.w3.org/TR/WGSL/#interpolation-type
interp_modifiers = {
	"constant":        "@interpolate(flat) ",
	"linear":          "@interpolate(perspective) ",
	"linear centroid": "@interpolate(perspective, centroid) ",
	"linear sample":   "@interpolate(perspective, sample) ",
	"linear noperspective":          "@interpolate(linear) ",
	"linear noperspective centroid": "@interpolate(linear, centroid) ",
	"linear noperspective sample":   "@interpolate(linear, sample) ",
}
# https://www.w3.org/TR/WGSL/#builtin-values
input_types = {
	**{f"v{i}":      (f"@location({i})", "float4") for i in range(16)},
	"position":      ("@builtin(position)", "float4"),
	"vertexid":      ("@builtin(vertex_index)", "uint"), 
	"instanceid":    ("@builtin(instance_index)", "uint"),
	"isfrontface":   ("@builtin(front_facing)", "bool"),
	"sampleidx":     ("@builtin(sample_index)", "uint"),
	"vCoverageMask": ("@builtin(sample_mask)", "uint"),
}
output_types = {
	**{f"o{i}":      (f"@location({i})", "float4") for i in range(16)},
	"position":      ("@builtin(position)", "float4"),
	"oMask":         ("@builtin(sample_mask)", "uint"),
	"oDepth":        ("@builtin(frag_depth)", "float"),
	"oDepthGreaterEqual": ("@builtin(frag_depth)", "float"), # fallback
	"oDepthLessEqual":    ("@builtin(frag_depth)", "float"), # fallback
}
# https://www.w3.org/TR/WGSL/#entry-point-attributes
stage_names = {
	"vs": "vertex", 
	"ps": "fragment", 
	"cs": "compute",
}